# Weather API
(Nodejs + Hapi + Axios)

## Documentation

### Install 

In the project directory, you must install:
```
npm install 
```

Configuration file
```
config.js
```
Edit this file to change 'host' and 'port' configs.

### Run

In the project directory, you can run:
```
npm start
```

It runs the app in the development mode.
Open http://localhost:8000 to view it in the browser.

See app logs at:
```
default.log
```

### API

Get weather data for a single city based on city name:
```
/api/city/v1?name={city}
```

Get weather data for multiple cities:
```
/api/city/v1?name={city1}&name={city2}&name={cityN}
```
Example usage (get weather data for 'Guarda' and 'Aveiro' cities):
```
/api/city/v1?name=Guarda&name=Aveiro
```