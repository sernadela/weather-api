'use strict';

const Hapi = require('@hapi/hapi');
const config = require('./config');

const server = Hapi.server({
    port: config.app.port,
    host: config.app.host,
    routes: {
        cors: true
    }
});

const init = async () => {

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

require('./routes/routes')(server);

init();