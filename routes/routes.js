const axios = require('axios');
const config = require('../config');
const log = require('log-to-file');

/**
 *  Make http request based on cities name
 * @param cities
 * @returns {Promise<unknown[]|*>}
 */
const getWeatherData = async (cities) => {
    try{
        let reqArr = []
        for (let i = 0; i < cities.length; i++) {
            reqArr.push(axios.get(config.api.url, {
                params: {
                    q: cities[i],
                    APPID: config.api.key,
                    units: config.api.units
                }
            }));
        }
        return await axios.all(reqArr);
    }catch (e) {
        log(e);
        return e
    }
}

/**
 * Returns cities weather parsed data
 * @param cities
 * @returns {Promise<{data, status: (number|number|string|"rejected"|"fulfilled"|*)}|{data: *[], status: number}>}
 */
const getWeather = async (cities) => {
    const res = await getWeatherData(cities);
    if(res.length>0){
        let data = []
        for (let i = 0; i < res.length; i++){
            log(res[i].request.path);
            data.push({
                name: res[i].data.name,
                temp: res[i].data.main.temp,
                sunrise: res[i].data.sys.sunrise,
                sunset: res[i].data.sys.sunset
            });
        }

        return {
            status: 200,
            data: data
        };
    }else{
        return {
            status: res.response.status,
            data: res.response.data
        };
    }
}

module.exports = function(server) {

    server.route({
        method: 'GET',
        path: '/',
        handler: (request, h) => {
            return {author: 'Pedro Sernadela'};
        }
    });

    server.route({
        method: 'GET',
        path: '/api/city/v1',
        handler: async (request, h) => {
            let cities = request.query.name;
            log(request.url.toString());

            // test if is a single request
            if(!Array.isArray(cities)){
                cities = [cities];
            }

            const data = await getWeather(cities);
            return data;
        }
    });
}