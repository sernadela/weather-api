const config = {
    app: {
        port: 8000,
        host: 'localhost'
    },
    api: {
        url: 'https://api.openweathermap.org/data/2.5/weather',
        key: '43eee79278e99739899e0c3422d7578a', // just to simplify. Secret key's must be ENV var
        units: 'metric'
    }
};

module.exports = config;